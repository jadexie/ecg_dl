from ecg_dl import read_data
import os
import numpy as np
import tensorflow as tf
import argparse
import pickle
from my_dl_lib import generic_dataset
from my_dl_lib import generic_neural_network
from my_dl_lib.generic_gan import generator
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt



def load_dataset_from_pickle(pickle_file_path):
    """

    :param pickle_file_path:
    :return:
    """
    pickle_in = open(pickle_file_path, "rb")
    print("Loading dataset from pickle file...")
    loaded_dataset = pickle.load(pickle_in)
    print("**Loaded done**")
    beats_train = loaded_dataset['Train_beats']
    train_tags = loaded_dataset['Train_tags']
    beats_val = loaded_dataset['Val_beats']
    validation_tags = loaded_dataset['Val_tags']
    beats_test = loaded_dataset['Test_beats']
    test_tags = loaded_dataset['Test_tags']
    return beats_train, train_tags, beats_val, validation_tags, beats_test, test_tags


def classifier(network_architecture_file, tensor_board_dir, save_model_dir, dropout_prob):
    """
    Create a classifier for the ECG classification problem which will be defined with the architecture file.
    :param tensor_board_dir:  directory to save the logs of the netwrok.
    :param network_architecture_file:
    :return:
    """
    ecg_nn = generic_neural_network.GenericNeuralNetwork(network_architecture_file, tensor_board_dir, model_save_dir=
                                                         save_model_dir, dropout_prob=dropout_prob)
    return ecg_nn


def create_dataset_according_to_aami(positive_class,
                                     length_of_each_sample=5, save_to_pickle=False, pickle_path=None,
                                     print_statistics=False, drop_other_beats=False):
    """
    :param original_data_dir:
    :param positive_class:
    :param length_of_each_sample:
    :param save_to_pickle:
    :param pickle_path:
    :return:
    """
    train_set, validation_set, test_set = read_data.divide_data_to_train_test_according_to_aami()

    validation_beats, validation_tags = read_data.merge_data_of_patients(validation_set, binary_pred=True,
                                                                         postivie_class=positive_class, aami_standarts=True)

    train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=True,
                                                               postivie_class=positive_class, aami_standarts=True)

    test_beats, test_tags = read_data.merge_data_of_patients(test_set, binary_pred=True,
                                                             postivie_class=positive_class, aami_standarts=True)

    if drop_other_beats:
        i = 0
        while i < len(train_beats):
            x = train_tags[i][1]
            if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                    and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
                # del train_beats[i]
                # del train_tags[i]
                train_beats = np.delete(train_beats, [i], axis=0)
                train_tags = np.delete(train_tags, [i], axis=0)
                continue
            i += 1

        i = 0
        while i < len(validation_beats):
            x = validation_tags[i][1]
            if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                    and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
                # del validation_beats[i]
                # del validation_tags[i]
                validation_beats = np.delete(validation_beats, [i], axis=0)
                validation_tags = np.delete(validation_tags, [i], axis=0)
                continue
            i += 1

        i = 0
        while i < len(test_beats):
            x = test_tags[i][1]
            if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                    and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
                # del test_beats[i]
                # del test_tags[i]
                test_beats = np.delete(test_beats, [i], axis=0)
                test_tags = np.delete(test_tags, [i], axis=0)
                continue
            i += 1

    if print_statistics:
        print("Train set statistics:")
        specific_train_tags = [x[1] for x in train_tags]
        specific_validation_tags = [x[1] for x in validation_tags]

        # 1. NOR - N:
        num_NOR = len([1 for x in specific_train_tags if x == 'N']) + len(
            [1 for x in specific_validation_tags if x == 'N'])

        # 2.LBBB - L:
        num_LBBB = len([1 for x in specific_train_tags if x == 'L']) + len(
            [1 for x in specific_validation_tags if x == 'L'])

        # 3. RBBB - R:
        num_RBBB = len([1 for x in specific_train_tags if x == 'R']) + len(
            [1 for x in specific_validation_tags if x == 'R'])

        # 4. AE - e
        num_AE = len([1 for x in specific_train_tags if x == 'e']) + len(
            [1 for x in specific_validation_tags if x == 'e'])

        # 5. NE - j :
        num_NE = len([1 for x in specific_train_tags if x == 'j']) + len(
            [1 for x in specific_validation_tags if x == 'j'])

        print(" NOR: %d || LBBB: %d || RBBB: %d || AE: %d || NE: %d " % (num_NOR, num_LBBB, num_RBBB, num_AE, num_NE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 6. AP - A:
        num_AP = len([1 for x in specific_train_tags if x == 'A']) + len(
            [1 for x in specific_validation_tags if x == 'A'])

        # 7. aAP - a:
        num_aAP = len([1 for x in specific_train_tags if x == 'a']) + len(
            [1 for x in specific_validation_tags if x == 'a'])

        # 8. NP - J:
        num_NP = len([1 for x in specific_train_tags if x == 'J']) + len(
            [1 for x in specific_validation_tags if x == 'J'])

        # 9. SP - S:
        num_SP = len([1 for x in specific_train_tags if x == 'S']) + len(
            [1 for x in specific_validation_tags if x == 'S'])

        print(" AP: %d || aAP: %d || NP: %d || SP: %d " % (num_AP, num_aAP, num_NP, num_SP))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 10. PVC - V:
        num_PVC = len([1 for x in specific_train_tags if x == 'V']) + len(
            [1 for x in specific_validation_tags if x == 'V'])

        # 11. VE - E:
        num_VE = len([1 for x in specific_train_tags if x == 'E']) + len(
            [1 for x in specific_validation_tags if x == 'E'])

        print(" PVC: %d || VE: %d " % (num_PVC, num_VE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 12. fVN - F:
        num_fVN = len([1 for x in specific_train_tags if x == 'F']) + len(
            [1 for x in specific_validation_tags if x == 'F'])

        print(" fVN: %d " % (num_fVN))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 13. P - /:
        num_P = len([1 for x in specific_train_tags if x == '/']) + len(
            [1 for x in specific_validation_tags if x == '/'])

        # 14. fPN - f:
        num_fPN = len([1 for x in specific_train_tags if x == 'f']) + len(
            [1 for x in specific_validation_tags if x == 'f'])

        # 15. U - Q:
        num_U = len([1 for x in specific_train_tags if x == 'Q']) + len(
            [1 for x in specific_validation_tags if x == 'Q'])

        print(" P: %d || fPN: %d || U: %d " % (num_P, num_fPN, num_U))
        print("_____________________________________________________")
        print("_____________________________________________________")

        num_others = len([1 for x in specific_train_tags if x != 'Q' and x != 'f' and x != '/' and x != 'F'
                          and x != 'E' and x != 'V' and x != 'S' and x != 'J' and x != 'a' and x != 'A' and x != 'j'
                          and x != 'e' and x != 'R' and x != 'L' and x != 'N']) + len([1 for x in
                                                                                       specific_validation_tags if x != 'Q' and x != 'f' and x != '/' and x != 'F'
                          and x != 'E' and x != 'V' and x != 'S' and x != 'J' and x != 'a' and x != 'A' and x != 'j'
                          and x != 'e' and x != 'R' and x != 'L' and x != 'N'])

        print("Number of others: ", num_others)
        print("Total number of beats in train:", len(specific_train_tags) + len(specific_validation_tags))
        print("Number agg: ", num_fPN + num_fVN + num_SP + num_NP + num_aAP + num_AP + num_NE + num_AE + num_RBBB + num_LBBB + num_NOR + num_P + num_PVC + num_U + num_VE)
        print("*****************************************************")

        print("Test set statistics:")
        specific_test_tags = [x[1] for x in test_tags]

        # 1. NOR - N:
        num_NOR = len([1 for x in specific_test_tags if x == 'N'])

        # 2.LBBB - L:
        num_LBBB = len([1 for x in specific_test_tags if x == 'L'])

        # 3. RBBB - R:
        num_RBBB = len([1 for x in specific_test_tags if x == 'R'])

        # 4. AE - e
        num_AE = len([1 for x in specific_test_tags if x == 'e'])

        # 5. NE - j :
        num_NE = len([1 for x in specific_test_tags if x == 'j'])

        print(" NOR: %d || LBBB: %d || RBBB: %d || AE: %d || NE: %d " % (num_NOR, num_LBBB, num_RBBB, num_AE, num_NE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 6. AP - A:
        num_AP = len([1 for x in specific_test_tags if x == 'A'])

        # 7. aAP - a:
        num_aAP = len([1 for x in specific_test_tags if x == 'a'])

        # 8. NP - J:
        num_NP = len([1 for x in specific_test_tags if x == 'J'])

        # 9. SP - S:
        num_SP = len([1 for x in specific_test_tags if x == 'S'])

        print(" AP: %d || aAP: %d || NP: %d || SP: %d " % (num_AP, num_aAP, num_NP, num_SP))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 10. PVC - V:
        num_PVC = len([1 for x in specific_test_tags if x == 'V'])

        # 11. VE - E:
        num_VE = len([1 for x in specific_test_tags if x == 'E'])

        print(" PVC: %d || VE: %d " % (num_PVC, num_VE))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 12. fVN - F:
        num_fVN = len([1 for x in specific_test_tags if x == 'F'])

        print(" fVN: %d " % (num_fVN))
        print("_____________________________________________________")
        print("_____________________________________________________")

        # 13. P - /:
        num_P = len([1 for x in specific_test_tags if x == '/'])

        # 14. fPN - f:
        num_fPN = len([1 for x in specific_test_tags if x == 'f'])

        # 15. U - Q:
        num_U = len([1 for x in specific_test_tags if x == 'Q'])

        print(" P: %d || fPN: %d || U: %d " % (num_P, num_fPN, num_U))
        print("_____________________________________________________")
        print("_____________________________________________________")

        num_others = len([1 for x in specific_test_tags if x != 'Q' and x != 'f' and x != '/' and x != 'F'
                          and x != 'E' and x != 'V' and x != 'S' and x != 'J' and x != 'a' and x != 'A' and x != 'j'
                          and x != 'e' and x != 'R' and x != 'L' and x != 'N'])

        print("Number of others: ", num_others)
        print("Total number of beats in test:", len(specific_test_tags))
        print("Number agg: ",
              num_fPN + num_fVN + num_SP + num_NP + num_aAP + num_AP + num_NE + num_AE + num_RBBB + num_LBBB + num_NOR
              + num_P + num_PVC + num_U + num_VE)

    validation_tags = np.array([int(x[0]) for x in validation_tags])
    train_tags = np.array([int(x[0]) for x in train_tags])
    test_tags = np.array([int(x[0]) for x in test_tags])

    train_beats = np.concatenate((train_beats, validation_beats), axis=0)
    print("shape train: ", train_beats.shape)
    train_tags = np.concatenate((train_tags, validation_tags), axis=0)
    print("shape tags", train_tags.shape)

    # Convert tags to one-hot vectors:
    val_dataset = generic_dataset.GenericDataSetIterator(validation_beats, validation_tags)
    validation_tags = val_dataset.convert_labels_to_one_hot_vector(2)
    train_dataset = generic_dataset.GenericDataSetIterator(train_beats, train_tags)
    train_tags = train_dataset.convert_labels_to_one_hot_vector(2)

    test_dataset = generic_dataset.GenericDataSetIterator(test_beats, test_tags)
    test_tags = test_dataset.convert_labels_to_one_hot_vector(2)

    if save_to_pickle:
        pickle_dict = {'Train_beats': np.array(train_beats), 'Train_tags': np.array(train_tags),
                       'Val_beats': np.array(validation_beats),
                       'Val_tags': np.array(validation_tags), 'Test_beats': np.array(test_beats),
                       'Test_tags': np.array(test_tags)}
        assert pickle_path is not None
        pickle_out = open(pickle_path, "wb")
        pickle.dump(pickle_dict, pickle_out)
        pickle_out.close()
        print("Saved data into pickle file %s", pickle_path)

    return np.array(train_beats), np.array(train_tags), np.array(validation_beats), np.array(validation_tags), \
           np.array(test_beats), np.array(test_tags)


def create_dataset(original_data_dir, number_of_classes, print_statistics=False, positive_class=None,
                   save_to_pickle=False, pickle_path=None):
    """
    Create a train, validation and test set of all the beats from all the patients in the dataset.
    beats from same patient will not be mixed between train validation and test sets.
    :param original_data_dir: directory which containts the original files downloaded from physionet
    :param number_of_classes: number of clasess we which to predict. beats that don't match to those classess will
    be removed.
    :param print_statistics: bool, if true then the function will also print statistics of each class in each set.
    :param positive_class:
    :param save_to_pickle
    :param pickle_path
    :return: train, validation and test sets.
    """

    # 1. Retrieve only the number of each patient and devide the patients to the 3 sets. Each patient has the same
    # number of beats.
    patient_numbers = read_data.get_list_of_all_patient_numbers(original_data_dir)
    train_set, validation_set, test_set = read_data.divide_data_to_train_val_test(patient_numbers, shuffle=False)

    if print_statistics:
        print("************************Statistics*************************************")
        print("Size of Train set: %d, Size of validation set: %d Size of Test set: %d"
              % (len(train_set), len(validation_set), len(test_set)))
        # Statistics of each set:
        print("*******************Train set statistics:*******************************")
        read_data.get_statistics_of_beat_types(train_set)

        print("******************Validation set statistics:***************************")
        read_data.get_statistics_of_beat_types(validation_set)

        print("*******************Test set statistics*********************************")
        read_data.get_statistics_of_beat_types(test_set)
        print("***********************************************************************")

    if positive_class is None:
        validation_beats, validation_tags = read_data.merge_data_of_patients(validation_set, binary_pred=False)

        train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=False)

        test_beats, test_tags = read_data.merge_data_of_patients(test_set, binary_pred=False)
    else:
        validation_beats, validation_tags = read_data.merge_data_of_patients(validation_set, binary_pred=True,
                                                                             postivie_class=positive_class)

        train_beats, train_tags = read_data.merge_data_of_patients(train_set, binary_pred=True,
                                                                   postivie_class=positive_class)

        test_beats, test_tags = read_data.merge_data_of_patients(test_set, binary_pred=True,
                                                                 postivie_class=positive_class)

    if print_statistics:
        print("************************More Statistics************************************")
        print("*******************Train set statistics:*******************")
        print("Number of N beats(== 1)", list(train_tags).count(1))
        print("Number of A beats(== 2)", list(train_tags).count(2))
        print("Number of V beats(== 3)", list(train_tags).count(3))
        print("Number of R beats(== 4)", list(train_tags).count(4))
        print("Number of Other beats(== 0)", list(train_tags).count(0))

        print("*******************Validation set statistics:*******************")
        print("Number of N beats(== 1)", list(validation_tags).count(1))
        print("Number of A beats(== 2)", list(validation_tags).count(2))
        print("Number of V beats(== 3)", list(validation_tags).count(3))
        print("Number of R beats(== 4)", list(validation_tags).count(4))
        print("Number of Other beats(== 0)", list(validation_tags).count(0))

        print("*******************Test set statistics:*******************")
        print("Number of N beats(== 1)", list(test_tags).count(1))
        print("Number of A beats(== 2)", list(test_tags).count(2))
        print("Number of V beats(== 3)", list(test_tags).count(3))
        print("Number of R beats(== 4)", list(test_tags).count(4))
        print("Number of Other beats(== 0)", list(test_tags).count(0))
        print("*************************************************************************")

        # Save plots of signals of each type:
        inds_of_A_in_train = [i for i, e in enumerate(list(train_tags)) if e == 2]
        inds_of_R_in_train = [i for i, e in enumerate(list(train_tags)) if e == 4]
        inds_of_V_in_train = [i for i, e in enumerate(list(train_tags)) if e == 3]

        inds_of_A_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 2]
        inds_of_R_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 4]
        inds_of_V_in_val = [i for i, e in enumerate(list(validation_tags)) if e == 3]

        inds_of_A_in_test = [i for i, e in enumerate(list(test_tags)) if e == 2]
        inds_of_R_in_test = [i for i, e in enumerate(list(test_tags)) if e == 4]
        inds_of_V_in_test = [i for i, e in enumerate(list(test_tags)) if e == 3]

        for j in inds_of_A_in_train[:10]:
            beat = train_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'A', 'train', str(j) + "_A_beat.png"))

        for j in inds_of_R_in_train[:10]:
            beat = train_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'R', 'train', str(j) + "_R_beat.png"))

        for j in inds_of_V_in_train[:10]:
            beat = train_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'V', 'train', str(j) + "_V_beat.png"))

        for j in inds_of_A_in_val[:10]:
            beat = validation_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'A', 'val', str(j) + "_A_beat.png"))

        for j in inds_of_R_in_val[:10]:
            beat = validation_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'R', 'val', str(j) + "_R_beat.png"))

        for j in inds_of_V_in_val[:10]:
            beat = validation_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'V', 'val', str(j) + "_V_beat.png"))

        for j in inds_of_A_in_test[:10]:
            beat = test_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'A', 'test', str(j) + "_A_beat.png"))

        for j in inds_of_R_in_test[:10]:
            beat = test_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'R', 'test', str(j) + "_R_beat.png"))

        for j in inds_of_V_in_test[:10]:
            beat = test_beats[j]
            plt.figure()
            plt.plot(beat)
            plt.savefig(os.path.join('samples', 'beat_types', 'V', 'test', str(j) + "_V_beat.png"))

    # Convert tags to one-hot vectors:
    val_dataset = generic_dataset.GenericDataSetIterator(validation_beats, validation_tags)
    validation_tags = val_dataset.convert_labels_to_one_hot_vector(number_of_classes)

    train_dataset = generic_dataset.GenericDataSetIterator(train_beats, train_tags)
    train_tags = train_dataset.convert_labels_to_one_hot_vector(number_of_classes)


    test_dataset = generic_dataset.GenericDataSetIterator(test_beats, test_tags)
    test_tags = test_dataset.convert_labels_to_one_hot_vector(number_of_classes)

    if save_to_pickle:
        pickle_dict = {'Train_beats': np.array(train_beats), 'Train_tags': np.array(train_tags),
                       'Val_beats': np.array(validation_beats),
                       'Val_tags': np.array(validation_tags), 'Test_beats': np.array(test_beats),
                       'Test_tags': np.array(test_tags)}
        assert pickle_path is not None
        pickle_out = open(pickle_path, "wb")
        pickle.dump(pickle_dict, pickle_out)
        pickle_out.close()
        print("Saved data into pickle file %s", pickle_path)

    return train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags


def add_data_from_gan(gan_meta_file, meta_file_directory, number_samples_to_generate, type_of_beat_generated,
                      number_of_classes):
    """

    :param gan_meta_file:
    :param number_samples_to_generate:
    :param type_of_beat_benerated:
    :return:
    """
    beats = generator.generate_data_from_trained_generator(number_samples_to_generate, gan_meta_file,
                                                           meta_file_directory)
    if number_of_classes == 5:
        if type_of_beat_generated == 'R':
            tags = np.array([4 for _ in range(len(beats))])
        elif type_of_beat_generated == "A":
            tags = np.array([2 for _ in range(len(beats))])
        elif type_of_beat_generated == "V":
            tags = np.array([3 for _ in range(len(beats))])
        else:
            pass
            # TODO: ADD suuport to more labels
    else:
        tags = np.array([1 for _ in range(len(beats))])

    dataset = generic_dataset.GenericDataSetIterator(beats, tags)
    tags = dataset.convert_labels_to_one_hot_vector(number_of_classes)
    return beats, tags


def train(ecg_classifier, train_beats, train_tags, validation_beats, validation_tags, number_of_iterations, batch_size):
    """
    Train an ecg_classifier of type GenericNeuralNetwork with the data provided.
    :param ecg_classifier: A network of type GenericNeuralNetwork with train method
    :param train_beats: ecg beats to be trained
    :param train_tags:
    :param validation_beats:
    :param validation_tags:
    :param number_of_iterations:
    :param batch_size:
    :return:
    """
    print("Begin Training: ")
    print("Number of iterations: %d, batch size: %d " % (number_of_iterations, batch_size))
    print("Size of train set: %d , size of validation set %d" % (len(train_beats), len(validation_beats)))

    ecg_classifier.train_tf(train_beats, train_tags, validation_beats, validation_tags, number_of_iterations,
                            batch_size, "adam", "cross_entropy", unbalanced_ratio=None)

    print("Training Done")


def evaluate_classifier(ecg_classifier, test_beats, test_tags, positive_class='TRAIN'):
    """
    Run the classifier on the test set, and print results and graphs
    :param ecg_classifier:
    :param test_beats:
    :param test_tags:
    :return:
    """
    assert ecg_classifier.session is not None
    print("***********************************************************************************************************")
    print("Evaluating results on test set:")
    accuracy = ecg_classifier.eval_accuracy(test_beats, test_tags)

    probability_predictions_of_test_set = ecg_classifier.get_probability_predictions(test_beats)
    binary_predictions_of_test_set = ecg_classifier.get_binary_predictions(test_beats)

    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions_of_test_set[:10])
    print("***********************************************************************************")

    print("***********************************************************************************")
    print("porbability preds first 10:", probability_predictions_of_test_set[:10])
    print("***********************************************************************************")
    if positive_class == 'TRAIN':
        print("**************************Results on Normal Beats:******************************************************"
              "***")
        normal_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[1] == 1]
        number_of_correct_preds_among_normal_beats = 0
        for j in normal_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 1:
                number_of_correct_preds_among_normal_beats += 1
        print("Among %d normal beats, classifier predicted correctly %d of them" %
              (len(normal_beats_ids), number_of_correct_preds_among_normal_beats))
        print("Accuracy on normal beats:", number_of_correct_preds_among_normal_beats / len(normal_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on A Beats:*********************************************************")
        A_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[2] == 1]
        number_of_correct_preds_among_A_beats = 0
        for j in A_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 2:
                number_of_correct_preds_among_A_beats += 1
        print("Among %d A beats, classifier predicted correctly %d of them" %
              (len(A_beats_ids), number_of_correct_preds_among_A_beats))
        print("Accuracy on A beats:", number_of_correct_preds_among_A_beats / len(A_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on V Beats:*********************************************************")
        V_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[3] == 1]
        number_of_correct_preds_among_V_beats = 0
        for j in V_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 3:
                number_of_correct_preds_among_V_beats += 1
        print("Among %d V beats, classifier predicted correctly %d of them" %
              (len(V_beats_ids), number_of_correct_preds_among_V_beats))
        print("Accuracy on V beats:", number_of_correct_preds_among_V_beats / len(V_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on R Beats:*********************************************************")
        R_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[4] == 1]
        number_of_correct_preds_among_R_beats = 0
        for j in R_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 4:
                number_of_correct_preds_among_R_beats += 1
        print("Among %d R beats, classifier predicted correctly %d of them" %
              (len(R_beats_ids), number_of_correct_preds_among_R_beats))
        print("Accuracy on R beats:", number_of_correct_preds_among_R_beats / len(R_beats_ids))
        print("********************************************************************************************************"
              "***")

        print("**************************Results on others Beats:******************************************************"
              "***")
        others_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[0] == 1]
        number_of_correct_preds_among_others_beats = 0
        for j in others_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 0:
                number_of_correct_preds_among_others_beats += 1
        print("Among %d others beats, classifier predicted correctly %d of them" %
              (len(others_beats_ids), number_of_correct_preds_among_others_beats))
        print("Accuracy on others beats:", number_of_correct_preds_among_others_beats / len(others_beats_ids))
        print("********************************************************************************************************"
              "***")
    else:
        positive_beat = positive_class.split('_')[1]
        print("**************************Results on " + positive_beat + " Beats:***************************************"
                                                                        "******************")
        positive_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[1] == 1]
        number_of_correct_preds_among_postive_beats = 0
        for j in positive_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 1:
                number_of_correct_preds_among_postive_beats += 1
        print("Among %d  %s beats, classifier predicted correctly %d of them" %
              (len(positive_beats_ids), positive_beat, number_of_correct_preds_among_postive_beats))
        print("Accuracy on " + positive_beat + " beats:", number_of_correct_preds_among_postive_beats /
              len(positive_beats_ids))
        print(
            "**********************************************************************************************************"
            "*")

        print("**************************Results on other Beats:******************************************************"
               "***")
        other_beats_ids = [i for i, e in enumerate(list(test_tags)) if e[0] == 1]
        number_of_correct_preds_among_other_beats = 0
        for j in other_beats_ids:
            pred = list(binary_predictions_of_test_set[j]).index(1)
            if pred == 0:
                number_of_correct_preds_among_other_beats += 1
        print("Among %d  other beats, classifier predicted correctly %d of them" %
              (len(other_beats_ids), number_of_correct_preds_among_other_beats))
        print("Accuracy on other beats:",
              number_of_correct_preds_among_other_beats / len(other_beats_ids))
        print(
            "**********************************************************************************************************"
            "*")

    print("***********************************************************************************************************")


def calc_precision_recall(ecg_classifier, x_data, x_tags, meta_file_name=None, meta_file_directory=None):
    """

    :param ecg_classifier
    :param x_data:
    :return:
    """
    print("Calculating precision recall on ecg data:")
    probability_predictions = ecg_classifier.get_probability_predictions(x_data)
    binary_predictions = ecg_classifier.get_binary_predictions(x_data)
    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions[:10])
    print("***********************************************************************************")
    print("***********************************************************************************")
    print("prob preds first 10:", probability_predictions[:10])
    print("***********************************************************************************")

    print("***********************************************************************************")
    print("truth first 10:", x_tags[:10])
    print("***********************************************************************************")

    ecg_classifier.print_precision_recall_graph(x_tags, probability_predictions, save_path='.')


def calc_roc_curve(ecg_classifier, x_data, x_tags, save_path, meta_file_name=None, meta_file_directory=None):
    """

    :param ecg_classifier:
    :param x_data:
    :param x_tags:
    :param save_path: where to save the .png file of the ROC curve.
    :return:
    """
    print("Calculating ROC curve on ecg data:")
    probability_predictions = ecg_classifier.get_probability_predictions(x_data)
    binary_predictions = ecg_classifier.get_binary_predictions(x_data)
    print("***********************************************************************************")
    print("binary preds first 10:", binary_predictions[:10])
    print("***********************************************************************************")
    print("***********************************************************************************")
    print("prob preds first 10:", probability_predictions[:10])
    print("***********************************************************************************")

    print("***********************************************************************************")
    print("truth first 10:", x_tags[:10])
    print("***********************************************************************************")
    ecg_classifier.calc_roc(x_tags, probability_predictions, save_path)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--network_architecture_file', type=str, help='path to file with the architecture of the '
                                                                        'network')
    parser.add_argument('--tensor_board_dir', type=str, help='directory to save log events')
    parser.add_argument('--saved_models_dir', type=str, help='directory to save model values')

    parser.add_argument('--use_gan', action='store_true', help='Use a generator and add synthetic data')
    parser.add_argument('--gan_type', type=str, default='A', help='Type of ECG class to generate',
                        choices=['A', 'R', 'V'])
    parser.add_argument('--gan_meta_file', type=str, help='The meta file of the GAN')
    parser.add_argument('--gan_meta_file_directory', type=str, help='The directory of the meta file of the gan')

    parser.add_argument('--mode', type=str, help='Which mode to apply the classifier',
                        choices=['TRAIN', 'TEST', 'TRAIN_V', 'TRAIN_R', 'TRAIN_A', 'TRAIN_N', 'TRAIN_O'],
                        default='TRAIN')

    parser.add_argument('--best_acc_validation_value', type=float, help='what was the best accuracy on the validation '
                                                                        'set when training')
    parser.add_argument('--loaded_graph_name', type=str, help='Name of the meta file of the trained LSTM')
    parser.add_argument('--meta_file_directory', type=str, help="Path to the directory of the meta file")

    parser.add_argument('--save_data_to_pickle', action='store_true', help='Save dataset into a pickle file')
    parser.add_argument('--pickle_path', type=str, help='pickle path to save or load the dataset', default=None)
    parser.add_argument('--use_data_from_pickle', action='store_true', help='Load dataset from oickle file')

    args = parser.parse_args()

    print("Save model dir:", args.saved_models_dir)
    print("Logs dir:", args.tensor_board_dir)
    print("Use Gan ? : ", args.use_gan)
    if args.use_gan:
        print("GAN type", args.gan_type)
    print("Load data from pickle file ? ", args.use_data_from_pickle)
    if args.use_data_from_pickle:
        print("Going to use data from pickle file:", args.pickle_path)

    if args.use_data_from_pickle:
        train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = \
            load_dataset_from_pickle(args.pickle_path)
    else:
        if args.mode == 'TRAIN_V':
            # Create dataset where: V beats are tagged as 1. all others are tagged as 0.
            train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = create_dataset(
                os.path.join('Data', 'MIT database'), number_of_classes=2,
                save_to_pickle=args.save_data_to_pickle,
                pickle_path=args.pickle_path, positive_class='V')
        elif args.mode == 'TRAIN_R':
            # Create dataset where: R beats are tagged as 1. all others are tagged as 0.
            train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = create_dataset(
                os.path.join('Data', 'MIT database'), number_of_classes=2,
                save_to_pickle=args.save_data_to_pickle,
                pickle_path=args.pickle_path, positive_class='R')
        elif args.mode == 'TRAIN_A':
            # Create dataset where: A beats are tagged as 1. all others are tagged as 0.
            train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = create_dataset(
                os.path.join('Data', 'MIT database'), number_of_classes=2,
                save_to_pickle=args.save_data_to_pickle,
                pickle_path=args.pickle_path, positive_class='A')
        elif args.mode == 'TRAIN_N':
            # Create dataset where: N beats are tagged as 1. all others are tagged as 0.
            train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = create_dataset(
                os.path.join('Data', 'MIT database'), number_of_classes=2,
                save_to_pickle=args.save_data_to_pickle,
                pickle_path=args.pickle_path, positive_class='N')
        elif args.mode == 'TRAIN_O':
            # Create dataset where: V beats are tagged as 1. all others are tagged as 0.
            train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = create_dataset(
                os.path.join('Data', 'MIT database'), number_of_classes=2,
                save_to_pickle=args.save_data_to_pickle,
                pickle_path=args.pickle_path, positive_class='O')
        else:
            train_beats, train_tags, validation_beats, validation_tags, test_beats, test_tags = create_dataset(
                os.path.join('Data', 'MIT database'), 5, save_to_pickle=args.save_data_to_pickle,
                pickle_path=args.pickle_path, positive_class=None)

    if args.use_gan:
        print("Adding synthetic %s beats:" % args.gan_type)
        if args.mode == 'TRAIN':
            number_of_classes = 5
        else:
            number_of_classes = 2
        gan_beats, gan_tags = add_data_from_gan(args.gan_meta_file, number_samples_to_generate=500,
                                                type_of_beat_generated=args.gan_type,
                                                meta_file_directory=args.gan_meta_file_directory,
                                                number_of_classes=number_of_classes)

        train_beats = np.concatenate((train_beats, gan_beats), axis=0)
        train_tags = np.concatenate((train_tags, gan_tags), axis=0)

        # Don't need the gan graph now, so clean it:
        tf.reset_default_graph()

    if args.mode != 'TEST':
        ecg_classifier = classifier(network_architecture_file=args.network_architecture_file,
                                    tensor_board_dir=args.tensor_board_dir,
                                    save_model_dir=args.saved_models_dir,
                                    dropout_prob=1)

        train(ecg_classifier, train_beats, train_tags, validation_beats, validation_tags, number_of_iterations=4000,
              batch_size=120)

        evaluate_classifier(ecg_classifier, test_beats, test_tags, positive_class=args.mode)

        calc_precision_recall(ecg_classifier, test_beats, test_tags, meta_file_name=None, meta_file_directory=None)

    else:
        # TODO: Complete
        pass