#!/bin/bash

echo "                                     " "#########" Auto script to Train GAN "#########"
echo
beat_types=(N S V F Q)

for b in ${beat_types[*]}; do

	echo "#########" Trainning GAN to generate $b beats "#####################"
	echo
	
	python3 ecg_gan.py --tensor_board_dir logs/aami/gan/${b} --saved_models_dir saved_models/aami/gan/${b} --generated_samples_dir samples/aami/gan/${b} --gan_type ${b} --database_type AAMI > ${b}_GAN.log
	
done