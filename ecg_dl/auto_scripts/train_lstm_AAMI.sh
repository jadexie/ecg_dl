#!/bin/bash

echo "                                     " "#########" Auto script to Train LSTM on ecg with Samples from GAN AAMI standart"#########"
echo
beat_types=(N S V F Q)

for b in ${beat_types[*]}; do

	echo "#########" Trainning LSTM to classifiy $b beats "#####################"
	echo
	sample_number_from_gan=(500 800 1000 1500 3000 5000 7000 10000 15000)

	echo "######" Train with additional 0 samples from GAN: "################"
	echo

	python3 LSTM_classifier.py --tensor_board_dir logs/aami/script/${b} --saved_models_dir saved_models/aami/script/${b} --beat_to_classify $b --mode TRAIN_${b} --database_type AAMI > ${b}.log

	for s in ${sample_number_from_gan[*]}; do

		echo "######" Train with additional $s samples from GAN: "################"
		echo
		
		python3 LSTM_classifier.py --tensor_board_dir logs/aami/script/${b}_${s}_with_gan --saved_models_dir saved_models/aami/script/${b}_${s}_with_gan --beat_to_classify $b --mode TRAIN_${b} --num_of_examples_to_add_from_gan $s --use_gan --gan_type ${b} --gan_meta_file saved_models/aami/gan/${b}/iter_1400 --gan_meta_file_directory saved_models/aami/gan/${b} --database_type AAMI > ${b}_${s}.log

		echo "##################################################################"
		echo
	done
done