from ecg_dl import read_data
import neurokit as nk
from biosppy import tools as st
import matplotlib.pyplot as plt
import numpy as np


def calculate_avg_values():
    """

    :return:
    """
    train_beats = create_dataset_with_features()

    N_beats = np.array([x for x in train_beats if x['label'][0] == 1])
    S_beats = np.array([x for x in train_beats if x['label'][1] == 1])
    V_beats = np.array([x for x in train_beats if x['label'][2] == 1])
    F_beats = np.array([x for x in train_beats if x['label'][3] == 1])
    Q_beats = np.array([x for x in train_beats if x['label'][4] == 1])

    N_beats_values = dict()
    N_beats_values['avg_r'] = np.average([x['cardiac_cycle'][x['R_peak_location']] for x in N_beats])
    N_beats_values['avg_t'] = np.average([x['cardiac_cycle'][x['T_wave_location']] for x in N_beats if x['T_wave_location'] < 216])
    N_beats_values['avg_p'] = np.average([x['cardiac_cycle'][x['P_wave_location']] for x in N_beats])
    N_beats_values['avg_q'] = np.average([x['cardiac_cycle'][x['Q_wave_location']] for x in N_beats])

    S_beats_values = dict()
    S_beats_values['avg_r'] = np.average([x['cardiac_cycle'][x['R_peak_location']] for x in S_beats])
    S_beats_values['avg_t'] = np.average([x['cardiac_cycle'][x['T_wave_location']] for x in S_beats if x['T_wave_location'] < 216])
    S_beats_values['avg_p'] = np.average([x['cardiac_cycle'][x['P_wave_location']] for x in S_beats])
    S_beats_values['avg_q'] = np.average([x['cardiac_cycle'][x['Q_wave_location']] for x in S_beats])

    V_beats_values = dict()
    V_beats_values['avg_r'] = np.average([x['cardiac_cycle'][x['R_peak_location']] for x in V_beats])
    V_beats_values['avg_t'] = np.average([x['cardiac_cycle'][x['T_wave_location']] for x in V_beats if x['T_wave_location'] < 216])
    V_beats_values['avg_p'] = np.average([x['cardiac_cycle'][x['P_wave_location']] for x in V_beats])
    V_beats_values['avg_q'] = np.average([x['cardiac_cycle'][x['Q_wave_location']] for x in V_beats])

    F_beats_values = dict()
    F_beats_values['avg_r'] = np.average([x['cardiac_cycle'][x['R_peak_location']] for x in F_beats])
    F_beats_values['avg_t'] = np.average([x['cardiac_cycle'][x['T_wave_location']] for x in F_beats if x['T_wave_location'] < 216])
    F_beats_values['avg_p'] = np.average([x['cardiac_cycle'][x['P_wave_location']] for x in F_beats])
    F_beats_values['avg_q'] = np.average([x['cardiac_cycle'][x['Q_wave_location']] for x in F_beats])

    Q_beats_values = dict()
    Q_beats_values['avg_r'] = np.average([x['cardiac_cycle'][x['R_peak_location']] for x in Q_beats])
    Q_beats_values['avg_t'] = np.average([x['cardiac_cycle'][x['T_wave_location']] for x in Q_beats if x['T_wave_location'] < 216])
    Q_beats_values['avg_p'] = np.average([x['cardiac_cycle'][x['P_wave_location']] for x in Q_beats])
    Q_beats_values['avg_q'] = np.average([x['cardiac_cycle'][x['Q_wave_location']] for x in Q_beats])

    return N_beats_values, S_beats_values, V_beats_values, F_beats_values, Q_beats_values


def create_dataset_with_features():
    """

    :return:
    """
    train_set, validation_set, test_set = read_data.divide_data_to_train_test_according_to_aami()
    train_set = train_set + validation_set

    train_beats = merge_data_of_patients(train_set)

    for beats_dict in train_beats:
        label = beats_dict['label']
        one_hot_label = [0 for _ in range(5)]
        one_hot_label[label] = 1
        beats_dict['label'] = one_hot_label

    return train_beats


def merge_data_of_patients(patient_numbers):
    """

    :param patient_numbers:
    :return:
    """
    first_iter = True
    merged_beats = np.array([])
    for p in patient_numbers:

        heart_beats = create_list_of_beats_for_patient(p)

        if first_iter:
            first_iter = False
            merged_beats = heart_beats
        else:
            merged_beats = np.concatenate((merged_beats, heart_beats), axis=0)

    return merged_beats


def convert_tag_to_aami_class(tag):
    """
    convert to 5 different classes:
    0 - N
    1 - S
    2 - V
    3 - F
    4 - Q
    :param tags:
    :return: ret.
    """

    x = tag
    if x == 'N' or x == 'L' or x == 'R' or x == 'e' or x == 'j':
        ret = 0
    elif x == 'A' or x == 'a' or x == 'J' or x == 'S':
        ret = 1
    elif x == "V" or x == 'E':
        ret = 2
    elif x == "F":
        ret = 3
    else:
        ret = 4
        if x != "Q" and x != '/' and x != 'f':
            print("Warning - uindentified beat")

    return ret


def extract_heart_beats_and_waves_from_ecg_signal(ecg_signal, r_peak_locations, tags, before=0.2, after=0.4,
                                                  sampling_rate=360):
    """

    :param ecg_signal: np array
    :param r_peak_locations:
    :param tags
    :param before: Window size to include before the R peak (seconds)
    :param after: Window size to include after the R peak (seconds).
    :param sampling_rate: Sampling frequency (Hz). 360 in MIT-BIH
    :return:
    """
    ecg_signal = np.array(ecg_signal)
    r_peak_locations = np.array(r_peak_locations)
    # convert seconds to samples
    before = int(before * sampling_rate)
    after = int(after * sampling_rate)

    len_of_signal = len(ecg_signal)

    heart_beats = []

    for ind, r_peak in enumerate(r_peak_locations):
        start = r_peak - before
        if start < 0:
            continue
        end = r_peak + after
        if end > len_of_signal - 1:
            break
        heart_beats_dict = {}
        heart_beat = np.array(ecg_signal[start:end])
        heart_beats_dict['cardiac_cycle'] = heart_beat
        r_local_location = before
        heart_beats_dict['R_peak_location'] = r_local_location
        heart_beats_dict['beat_type'] = tags[ind]
        heart_beats_dict['label'] = convert_tag_to_aami_class(tags[ind])
        x = heart_beats_dict['beat_type']
        if x != 'Q' and x != 'f' and x != '/' and x != 'F' and x != 'E' and x != 'V' and x != 'S' and x != 'J' \
                and x != 'a' and x != 'A' and x != 'j' and x != 'e' and x != 'R' and x != 'L' and x != 'N':
            # print("Skipping because beat type is: ", x)
            continue

        # Find t wave location:
        if ind < len(r_peak_locations) - 1:
            middle = (r_peak_locations[ind + 1] - r_peak) / 2
            quarter = middle / 2
            epoch = ecg_signal[int(r_peak + quarter):int(r_peak + middle)]
            # t_wave_global_location = int(r_peak + quarter) + np.argmax(epoch)
            t_wave_local_location = int(r_local_location + quarter) + np.argmax(epoch)
            if t_wave_local_location > before + after:
                print("Location out of bounds: ", t_wave_local_location)
            heart_beats_dict['T_wave_location'] = t_wave_local_location
        else:
            print("Cont")
            continue
        # Find p wave location:
        if ind > 0:
            middle = (r_peak - r_peak_locations[ind - 1]) / 2
            quarter = middle / 2
            epoch = ecg_signal[int(r_peak - middle):int(r_peak - quarter)]
            p_wave_global_location = int(r_peak - quarter) + np.argmax(epoch)
            p_wave_local_location = int(r_local_location - quarter) + np.argmax(epoch)
            heart_beats_dict['P_wave_location'] = p_wave_local_location

            # Find q wave location:
            epoch = ecg_signal[int(p_wave_global_location):int(r_peak_locations[r_peak_locations >
                                                                                p_wave_global_location][0])]

            # q_wave_global_location = p_wave_global_location + np.argmin(epoch)
            q_wave_local_location = p_wave_local_location + np.argmin(epoch)
            heart_beats_dict['Q_wave_location'] = q_wave_local_location

            # Try to correct P:
            p_wave_local_loc = np.argmax(heart_beat[:q_wave_local_location])
            heart_beats_dict['P_wave_location'] = p_wave_local_loc
        heart_beats.append(heart_beats_dict)

    return heart_beats


def extract_features_from_ecg_signal(ecg_signal_text_file_path, ecg_header_file_path):
    """

    :param ecg_signal_text_file_path:
    :param ecg_header_file_path:
    :return:
    """
    time, voltage1, voltage2, tags_time, tags, r_peaks_indexes = read_data.read_data_from_single_patient('100')
    processed_ecg = nk.ecg_process(voltage1, None, sampling_rate=360)
    # print(processed_ecg)
    t_waves_indices = processed_ecg['ECG']['T_Waves']
    r_peaks_indices = processed_ecg['ECG']['R_Peaks']
    p_waves_indices = processed_ecg['ECG']['P_Waves']
    q_waves_indices = processed_ecg['ECG']['Q_Waves']

    t_waves_indices_to_plot = [x for x in t_waves_indices if x <= 2000]
    p_waves_indices_to_plot = [x for x in p_waves_indices if x <= 2000]
    q_waves_indices_to_plot = [x for x in q_waves_indices if x <= 2000]
    r_waves_indices_to_plot = [x for x in r_peaks_indices if x <= 2000]

    time_to_plot = np.array(time[0:2000])
    voltage_to_plot = np.array(voltage1[0:2000])
    plt.plot(time_to_plot, voltage_to_plot)
    plt.plot(time_to_plot[t_waves_indices_to_plot], voltage_to_plot[t_waves_indices_to_plot], 'ro', color='green')
    plt.plot(time_to_plot[p_waves_indices_to_plot], voltage_to_plot[p_waves_indices_to_plot], 'ro', color='red')
    plt.plot(time_to_plot[q_waves_indices_to_plot], voltage_to_plot[q_waves_indices_to_plot], 'ro', color='blue')
    plt.plot(time_to_plot[r_waves_indices_to_plot], voltage_to_plot[r_waves_indices_to_plot], 'ro', color='yellow')
    plt.show()


def create_list_of_beats_for_patient(patient_number):
    """

    :param patient_number:
    :return:
    """
    time, voltage1, voltage2, tags_time, tags, r_peaks_indices_from_header_file = \
        read_data.read_data_from_single_patient(patient_number)

    '''
    processed_ecg = nk.ecg_process(voltage1, None, sampling_rate=360)
    # print(processed_ecg)
    t_waves_indices = processed_ecg['ECG']['T_Waves']
    r_peaks_indices = processed_ecg['ECG']['R_Peaks']
    p_waves_indices = processed_ecg['ECG']['P_Waves']
    q_waves_indices = processed_ecg['ECG']['Q_Waves']
    heart_beats_df = processed_ecg['ECG']['Cardiac_Cycles']
    p_waves_to_plot = np.array([x for x in p_waves_indices if x < 500])
    r_waves_to_plot = np.array([x for x in r_peaks_indices if x < 500])
    ecg_plot = np.array(voltage1[:500])
    plt.plot(ecg_plot)
    plt.plot(p_waves_to_plot, ecg_plot[p_waves_to_plot], 'ro', color='green')
    plt.plot(r_waves_to_plot, ecg_plot[r_waves_to_plot], 'ro', color='red')
    plt.show()
    '''
    '''
    heart_beats_orig = extract_heart_beats_and_waves_from_ecg_signal(voltage1, r_peaks_indices_from_header_file,
                                                                     tags, before=0.2, after=0.4, sampling_rate=360)
    '''
    # filter signal:
    order = int(0.3 * 360)
    filtered, _, _ = st.filter_signal(signal=voltage1,
                                      ftype='FIR',
                                      band='bandpass',
                                      order=order,
                                      frequency=[3, 45],
                                      sampling_rate=360)

    heart_beats_filtered = extract_heart_beats_and_waves_from_ecg_signal(filtered, r_peaks_indices_from_header_file,
                                                                         tags, before=0.2, after=0.4, sampling_rate=360)

    return heart_beats_filtered


def plot_heart_beat(heart_beats, num):
    """

    :param heart_beats:
    :param num:
    :return:
    """
    hb = heart_beats[num]['cardiac_cycle']
    r = heart_beats[num]['R_peak_location']
    t = heart_beats[num]['T_wave_location']
    p = heart_beats[num]['P_wave_location']
    q = heart_beats[num]['Q_wave_location']
    plt.plot(hb)
    plt.plot(r, hb[r], 'ro', color='green')
    plt.plot(p, hb[p], 'ro', color='yellow')
    plt.plot(q, hb[q], 'ro', color='blue')
    plt.plot(t, hb[t], 'ro', color='red')
    plt.show()


if __name__ == "__main__":
    # extract_features_from_ecg_signal(None, None)
    # create_list_of_beats_for_patient('101')
    # create_dataset_with_features()
    calculate_avg_values()